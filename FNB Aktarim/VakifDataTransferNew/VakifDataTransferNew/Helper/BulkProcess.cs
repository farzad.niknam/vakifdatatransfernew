﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.Transactions;

namespace VakifDataTransferNew.Helper
{
    public static class BulkProcess
    {


        private static SqlConnectionStringBuilder _Builder;

        public static SqlConnectionStringBuilder Builder
        {
            get { return _Builder; }
            set { _Builder = value; }
        }


        static BulkProcess()
        {
            Builder = SqlConnectionBuilder.GetFMSConnectionString();
        }


        public static void BulkInsert(Dictionary<string, DataTable> dict)
        {
            var connectionString = SqlConnectionBuilder.GetConnectionString("FMSEntities");
            using (var ts = new TransactionScope())
            {
                using (var sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open(); // ensure to open it before SqlBulkCopy can open it in another transactionscope.

                    foreach (var item in dict)
                    {
                        using (var bulkCopy = new SqlBulkCopy(sqlCon))
                        {
                            bulkCopy.DestinationTableName = item.Key;
                            bulkCopy.WriteToServer(item.Value);
                        }
                    }
                }

                ts.Complete();
            }
        }


        public static void BulkInsert(DataTable dt, string tableName)
        {
            using (var ts = new TransactionScope())
            {
                using (var sqlCon = new SqlConnection(Builder.ConnectionString))
                {
                    sqlCon.Open(); // ensure to open it before SqlBulkCopy can open it in another transactionscope.
                    using (var bulkCopy = new SqlBulkCopy(sqlCon))
                    {
                        bulkCopy.DestinationTableName = tableName;
                        bulkCopy.WriteToServer(dt);

                    }
                }

                ts.Complete();
            }

        }

    }
}
