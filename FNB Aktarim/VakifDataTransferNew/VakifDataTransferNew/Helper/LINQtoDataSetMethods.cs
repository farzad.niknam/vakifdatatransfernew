﻿using System.Collections.Generic;
using System.Data;

namespace VakifDataTransferNew.Helper
{
    public static class LINQtoDataSetMethods
    {

        /// <summary>
        /// Verilen bir obje Listesini DataTable a ceviren Microst Metodudur.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static DataTable KopyalaToDataTable<T>(this IEnumerable<T> source)
        {
            var dt = new ObjectShredder<T>().Shred(source, null, null);

            List<string> VirtualList = new List<string>();
            //
            foreach (DataColumn item in dt.Columns)
            {
                var isVirtual = typeof(T).GetProperty(item.ColumnName).GetGetMethod().IsVirtual;
                if (!isVirtual)
                    continue;

                VirtualList.Add(item.ColumnName);
            }

            VirtualList.ForEach(i =>
            {

                dt.Columns.Remove(i.ToString());

            });

            dt.AcceptChanges();

            return dt;

        } //end method.





        public static DataTable KopyalaToDataTable<T>(this IEnumerable<T> source, DataTable table, LoadOption? options)
        {

            return new ObjectShredder<T>().Shred(source, table, options);

        } //end method.

    }
}
