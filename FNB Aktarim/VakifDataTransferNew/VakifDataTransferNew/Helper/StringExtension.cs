﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VakifDataTransferNew.Helper
{
    public static class StringExtension
    {
        public static string ConvertTurkishCharToEnglish(this string str)
        {
            var newStr = String.Join("", str.Normalize(NormalizationForm.FormD)
                               .Where(c => char.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark))
                               .Replace("ı", "i");
            return newStr;
        }
    }
}
