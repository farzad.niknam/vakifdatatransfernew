﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VakifDataTransferNew.Helper
{
    public static class SqlConnectionBuilder
    {

        public static string GetConnectionString(string keyName)
        {
            try
            {
                var s = System.Configuration.ConfigurationManager.ConnectionStrings[keyName].ConnectionString;
                var e = new System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder(s);
                var connectionString = e.ProviderConnectionString;

                return connectionString;
            }
            catch (Exception exp)
            {
                throw;
            }
        }

        public static SqlConnectionStringBuilder GetFMSConnectionString()
        {
            try
            {

                var builder = new SqlConnectionStringBuilder()
                {
                    DataSource = @"167.86.94.208\AVILOGIC17",
                    //AttachDBFilename = $@"{dataFolder.FullName}\DapperSample.mdf",
                    //IntegratedSecurity = true,
                    InitialCatalog = "FMS",
                    ConnectTimeout = 30,
                    UserID = "sa",
                    Password = "Qet13579",
                    ApplicationName = "Dapper.Samples.Advanced"

                };

                return builder;

            }
            catch (Exception exp)
            {

                throw exp;
            }
        }

    }
}
