﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VakifDataTransferNew.TransferFiles;

namespace VakifDataTransferNew
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.ShowDialog();
                //
                var path = openFileDialog1.FileName;
                //
                if (path.Trim() == "")
                {
                    MessageBox.Show("Please Select a File", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                var extenstion = Path.GetExtension(path);
                //
                if (!extenstion.ToLower().Contains("csv"))
                {
                    MessageBox.Show("Please select 'CSV' Files", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }


                PersonTransfer.TransferInfoToPersone(path);
                //EmailTransfer.AddEmails(path);
                //// AddressTransfer.AddAddresses(path);
                // PhoneTransfer.AddPhones(path);
                //SubscriptionConstantTransfer.TransferInfoToSubscriptionConstant(path);
                //SubscriptionPeriodFeeTransfer.TransferInfoToSubscriptionPeriodFee(path);

                //OrganisationTransfer.TransferInfoToOrganisation(path);
                // JobInformationTransfer.TransferInfoToJobInfo(path);
                // MembershipTransfer.TransferInfoToMembership(path);
                // BankAccountTransfer.TransferInfoToBankAccount(path);
                //OfftimesTransfer.TransferInfoToOfftimes(path);

                MessageBox.Show("Succeed");
            }
            catch (Exception exp)
            {
                MessageBox.Show("Error");
                // throw exp;
            }
        }
    }
}
