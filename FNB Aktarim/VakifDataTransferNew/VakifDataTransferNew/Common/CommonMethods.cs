﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VakifDataTransferNew.Common
{
    public static class CommonMethods
    {

        public static int GetMonthBetweenTwoDate(DateTime startDate, DateTime endDate)
        {

            var diff = ((endDate.Year - startDate.Year) * 12) + endDate.Month - startDate.Month;
            return diff;

        }


        public static double GetMonthBetweenTwoDate2(DateTime startDate, DateTime endDate)
        {

            var diff = endDate.Subtract(startDate).Days / (365.25 / 12);
            return diff;

        }

    }
}
