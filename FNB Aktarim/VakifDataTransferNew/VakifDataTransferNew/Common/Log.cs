﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VakifDataTransferNew.Common
{
    public static class Log
    {

        public static void WriteToFile(string path, string fileName, string message, bool append)
        {
            var fullPath = Path.Combine(path, fileName);
            //
            if(!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }


            var sw = new StreamWriter(fullPath, append);
            sw.WriteLine(message);
            sw.Close();

        }

    }
}
