﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VakifDataTransferNew.Common;
using VakifDataTransferNew.Helper;
using VakifDataTransferNew.Model;

namespace VakifDataTransferNew.TransferFiles
{
    public static class MembershipTransfer
    {

        public static void TransferInfoToMembership(string path)
        {
            try
            {

                Log.WriteToFile(@"C:\Log", "Membership.txt", $"", false);


                using (var _db = new FMSEntities())
                {

                    var lines = File.ReadLines(path).ToList();
                    var count = 0;


                    var jobInformations = _db.JobInformations.AsNoTracking().ToList();
                    //
                    var memberships = new List<Membership>();
                    var membershipMemos = new List<MembershipMemo>();
                    var distinctList = new List<string>();
                    //
                    lines.ForEach(l =>
                    {
                        try
                        {

                            count++;
                            if (count == 1)
                            {
                                return;
                            }


                            var arrayList = l.Split(';');
                            var registerNo = arrayList[0].ToString().Trim();
                            //
                            var jobInformationEntity = jobInformations.FirstOrDefault(x => x.RegisterNo == registerNo);
                            //
                            if (jobInformationEntity == null)
                            {
                                Log.WriteToFile(@"C:\Log", "Membership.txt", $"RegisterNo : {registerNo} Bulunamadi - Date : {DateTime.Now.Date}", true);
                                return;
                            }


                            var numberIsAdded = distinctList.FirstOrDefault(x => x == registerNo);
                            //
                            if (numberIsAdded == null)
                                distinctList.Add(registerNo);



                            var uyelikAdi = arrayList[1].ToLower().ToString().Trim();
                            //
                            var active = arrayList[2].ToString() != "" ? Convert.ToInt32(arrayList[2].ToString()) : 0;
                            var isActive = active == 1 ? true : false;


                            var startDate = arrayList[3].ToString().Trim();
                            var endDate = arrayList[4].ToString().Trim();
                            var paymentDay = arrayList[5].ToString().Trim();


                            var membershipMemo = new MembershipMemo();
                            //
                            membershipMemo.RegisterNo = registerNo;
                            membershipMemo.MembershipType = uyelikAdi;
                            membershipMemo.isActive = isActive;
                            membershipMemo.StartDate = startDate != "" ? Convert.ToDateTime(startDate) : new DateTime(1753, 1, 1);
                            //
                            //if (endDate != "")
                            //    membershipMemo.EndDate = Convert.ToDateTime(endDate);

                            membershipMemo.JobInformationId = jobInformationEntity.Id;
                            membershipMemo.PaymentDay = paymentDay != "" ? Convert.ToInt32(paymentDay) : 0;

                            if (uyelikAdi.Contains("50"))
                                membershipMemo.membershipSettingId = new Guid("bde6cee5-bb1a-4a9d-a993-ae06ef024b41");
                            else
                                membershipMemo.membershipSettingId = new Guid("fb64ac03-b63a-4de6-b3f8-214a9824d2fe");


                            membershipMemos.Add(membershipMemo);

                        }
                        catch (Exception exp1)
                        {

                            Log.WriteToFile(@"C:\Log", "Membership.txt", $"{l}", true);
                            throw exp1;
                        }

                    });


                    var lastDate = DateTime.MinValue;
                    //
                    distinctList.ForEach(q =>
                    {

                        var membershipList = membershipMemos.Where(x => x.RegisterNo == q).ToList();
                        //
                        for (int i = 0; i < membershipList.Count; i++)
                        {
                            MembershipMemo membershipMemo = null;
                            if(i == 0)
                            {
                                membershipMemo = membershipList.FirstOrDefault(x => x.MembershipType.ToLower().Contains("asil"));
                                //
                                if (membershipMemo == null)
                                    continue;

                                lastDate = membershipMemo.StartDate;
                            }
                            else
                            {
                                membershipMemo = membershipList.FirstOrDefault(x => x.MembershipType.ToLower().Contains(i.ToString()));
                                //
                                if (membershipMemo == null)
                                    continue;

                                if (lastDate.Date == membershipMemo.StartDate.Date)
                                {
                                    lastDate = membershipMemo.StartDate.AddMinutes(1);
                                }
                                else
                                    lastDate = membershipMemo.StartDate;
                            }


                            var membership = new Membership();
                            //
                            membership.Id = Guid.NewGuid();
                            membership.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
                            membership.CreatedDate = DateTime.Now.Date;
                            membership.IsActive = membershipMemo.isActive;
                            //
                            if (membershipMemo.EndDate != null)
                                membership.EndDate = membershipMemo.EndDate;


                            membership.StartDate = lastDate;
                            membership.JobInformationId = membershipMemo.JobInformationId;
                            membership.MembershipSettingId = membershipMemo.membershipSettingId;
                            membership.PaymentDay = membershipMemo.PaymentDay;

                            memberships.Add(membership);

                        }


                        var membershipMemo50 = membershipList.FirstOrDefault(x => x.MembershipType.ToLower().Contains("50"));
                        //
                        if(membershipMemo50 != null)
                        {

                            var membership = new Membership();
                            //
                            membership.Id = Guid.NewGuid();
                            membership.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
                            membership.CreatedDate = DateTime.Now.Date;
                            membership.IsActive = membershipMemo50.isActive;
                            //
                            if (membershipMemo50.EndDate != null)
                                membership.EndDate = membershipMemo50.EndDate;


                            if (lastDate.Date == membershipMemo50.StartDate.Date)
                            {
                                lastDate = membershipMemo50.StartDate.AddMinutes(1);
                            }
                            else
                                lastDate = membershipMemo50.StartDate;

                            membership.StartDate = lastDate;
                            membership.JobInformationId = membershipMemo50.JobInformationId;
                            membership.MembershipSettingId = membershipMemo50.membershipSettingId;
                            membership.PaymentDay = membershipMemo50.PaymentDay;

                            memberships.Add(membership);

                        }



                    });


                    var dtMemberships = memberships.KopyalaToDataTable<Membership>();
                    var dict = new Dictionary<string, DataTable>();
                    dict.Add("Memberships", dtMemberships);
                    BulkProcess.BulkInsert(dict);

                    //_db.Memberships.AddRange(memberships);
                    //_db.SaveChanges();
                }

            }
            catch (Exception exp)
            {

            }
        }

    }



    public class MembershipMemo
    {
        public string RegisterNo { get; set; }
        public string MembershipType { get; set; }
        public bool isActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int PaymentDay { get; set; }
        public Guid JobInformationId { get; set; }
        public Guid membershipSettingId { get; set; }

    }
}
