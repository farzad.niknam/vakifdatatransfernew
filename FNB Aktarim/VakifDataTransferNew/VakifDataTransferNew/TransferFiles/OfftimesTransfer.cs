﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VakifDataTransferNew.Common;
using VakifDataTransferNew.Helper;
using VakifDataTransferNew.Model;

namespace VakifDataTransferNew.TransferFiles
{
    public static class OfftimesTransfer
    {

        public static void TransferInfoToOfftimes(string path)
        {
            try
            {

                Log.WriteToFile(@"C:\Log", "BankAccount.txt", $"", false);


                using (var _db = new FMSEntities())
                {

                    var lines = File.ReadLines(path).ToList();
                    var count = 0;


                    var jobInformations = _db.JobInformations.AsNoTracking().ToList();
                    var branches = _db.Branches.AsNoTracking().ToList();
                    var memberships = _db.Memberships.AsNoTracking().ToList();

                    var offTimes = new List<OffTime>();
                    //
                    lines.ForEach(l =>
                    {
                        try
                        {

                            count++;
                            if (count == 1)
                            {
                                return;
                            }




                            var arrayList = l.Split(';');
                            var registerNo = arrayList[0].ToString().Trim();
                            //
                            var jobInformationEntity = jobInformations.FirstOrDefault(x => x.RegisterNo.ToLower().Trim() == registerNo.ToLower().Trim());
                            //
                            if (jobInformationEntity == null)
                            {
                                Log.WriteToFile(@"C:\Log", "BankAccount.txt", $"JobInfo RegisterNo : {registerNo} Bulunamadi", true);
                                // return;
                            }

                            var currentMembers = memberships.Where(q => q.JobInformationId == jobInformationEntity.Id).ToList();

                            foreach (var membership in currentMembers)
                            {
                                var offtime = new OffTime();
                                offtime.Id = Guid.NewGuid();
                                offtime.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
                                offtime.CreatedDate = DateTime.Now.Date;
                                offtime.IsActive = true;

                                offtime.BeginDate = arrayList[1] != null &&
                                                    arrayList[1].ToString().Trim() != "" &&
                                                    arrayList[1].ToString().Trim() != "NULL" ? Convert.ToDateTime(arrayList[1]) : new DateTime(1753, 1, 1);

                                offtime.EndDate = arrayList[2] != null &&
                                                  arrayList[2].ToString().Trim() != "" &&
                                                  arrayList[2].ToString().Trim() != "NULL" ? Convert.ToDateTime(arrayList[2]) : new DateTime(1753, 1, 1);

                                offtime.MembershipId = membership.Id;
                                offtime.OffTimeTypeId = new Guid("e25d07af-8a41-4b05-a7a6-debc305cd546");// Ucretsiz

                                offTimes.Add(offtime);
                            }
                            

                        }
                        catch (Exception exp1)
                        {

                        }

                    });

                    try
                    {

                        var dtOfftime = offTimes.KopyalaToDataTable<OffTime>();
                        var dict = new Dictionary<string, DataTable>();
                        dict.Add("OffTimes", dtOfftime);
                        BulkProcess.BulkInsert(dict);

                        //_db.BankAccounts.AddRange(bankAccounts);
                        //_db.SaveChanges();
                    }
                    catch (Exception exp)
                    {

                        throw exp;
                    }
                }

            }
            catch (Exception exp)
            {

            }
        }

    }
}
