﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VakifDataTransferNew.Common;
using VakifDataTransferNew.Helper;
using VakifDataTransferNew.Model;

namespace VakifDataTransferNew.TransferFiles
{
    public static class OrganisationTransfer
    {
        public static void TransferInfoToOrganisation(string path)
        {
            try
            {

                Log.WriteToFile(@"C:\Log", "Organizasion.txt", $"", false);

                using (var _db = new FMSEntities())
                {

                    var list = new List<Organisation>();
                    var lines = File.ReadLines(path).ToList();
                    var count = 0;

                    lines.ForEach(l =>
                    {
                        try
                        {

                            count++;
                            if (count == 1)
                            {
                                return;
                            }

                            var arrayList = l.Split(';');
                            var organisationCodes = arrayList[0].ToString().Split('/');
                            var organisationNames = arrayList[1].ToString().Split('/');
                            //
                            if(organisationNames.Count() != 4)
                            {
                                Log.WriteToFile(@"C:\Log", "Organizasion.txt", $"{arrayList[1].ToString()};{count}", true);
                                return;
                            }


                            var parentGuid = Guid.Empty;
                            for (int i = 0; i < 4; i++)
                            {
                                var organisationName = organisationNames[i].ToString();
                                var organisationCode = organisationCodes[i].ToString();
                                //
                                if (i == 0)
                                {
                                    if (organisationName == "-")
                                        organisationName = "Belirtilmemiş";
                                }
                                else if (organisationName == "-")
                                    break;

                                var organisation = new Organisation();
                                //
                                var id = Guid.NewGuid();
                                organisation.Id = id;

                                if (parentGuid != Guid.Empty)
                                    organisation.UpperId = parentGuid;

                                organisation.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
                                organisation.CreatedDate = DateTime.Now.Date;
                                organisation.IsActive = true;
                                //
                                organisation.Code = organisationCode;
                                organisation.Name = organisationName;
                                organisation.PaymentDay = 8;
                                organisation.TypeId = new Guid("37a91398-2353-427d-bd3b-747b1acf2a1b"); // Belirtilmemiş

                                parentGuid = id;

                                list.Add(organisation);

                            }


                        }
                        catch (Exception exp1)
                        {

                        }

                    });

                    var dtOrganisation = list.KopyalaToDataTable<Organisation>();
                    var dict = new Dictionary<string, DataTable>();
                    dict.Add("Organisations", dtOrganisation);
                    BulkProcess.BulkInsert(dict);

                    //_db.Organisations.AddRange(list);
                    //_db.SaveChanges();

                }
            }
            catch (Exception exp)
            {

            }
        }
    }
}

