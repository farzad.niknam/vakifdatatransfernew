﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VakifDataTransferNew.Common;
using VakifDataTransferNew.Model;
using VakifDataTransferNew.Common;
using VakifDataTransferNew.Model;

namespace VakifDataTransferNew.TransferFiles
{
    public static class SubscriptionPeriodFeeTransfer
    {
        public static void TransferInfoToSubscriptionPeriodFee(string path)
        {
            try
            {

                Log.WriteToFile(@"C:\Log", "SubscriptionPeriodFee.txt", $"", false);


                using (var _db = new FMSEntities())
                {

                    var subscriptionConstants = _db.SubscriptionConstants.ToList();


                    var lines = File.ReadLines(path).ToList();
                    var count = 0;

                    var subscriptionPeriodFees = new List<SubscriptionPeriodFee>();
                    var subscriptionMonths = new List<SubscriptionMonth>();
                    //
                    lines.ForEach(l =>
                    {
                        try
                        {

                            count++;
                            if (count == 1)
                            {
                                return;
                            }

                            var arrayList = l.Split(';');

                            var multiplierStr = arrayList[0].ToString().Trim();
                            var startDateStr = arrayList[1].ToString().Trim();
                            //
                            if (startDateStr == "")
                            {
                                Log.WriteToFile(@"C:\Log", "SubscriptionPeriodFee.txt", $"StartDateStr : {startDateStr} Bulunamadi", true);
                                return;
                            }

                            var endDateStr = arrayList[2].ToString().Trim();
                            //
                            if (endDateStr == "")
                            {
                                Log.WriteToFile(@"C:\Log", "SubscriptionPeriodFee.txt", $"EndDateStr : {endDateStr} Bulunamadi", true);
                                return;
                            }


                            var multiplier = multiplierStr != "" ? Convert.ToDouble(multiplierStr) : 0d;
                            //
                            var subscriptionConstantEntity = subscriptionConstants.FirstOrDefault(x => x.Katsayi == multiplier);
                            //
                            if (subscriptionConstantEntity == null)
                            {
                                Log.WriteToFile(@"C:\Log", "SubscriptionPeriodFee.txt", $"Multiplier : {multiplier} - StartDateStr {startDateStr} - EndDateStr : {endDateStr} Bulunamadi", true);
                                return;
                            }


                            var startDate = Convert.ToDateTime(startDateStr);
                            var endDate = Convert.ToDateTime(endDateStr);


                            var subscriptionPeriodFee = new SubscriptionPeriodFee();
                            //
                            var subscriptionPeriodFeeId = Guid.NewGuid();
                            subscriptionPeriodFee.Id = subscriptionPeriodFeeId;
                            subscriptionPeriodFee.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
                            subscriptionPeriodFee.CreatedDate = DateTime.Now.Date;
                            subscriptionPeriodFee.IsActive = true;
                            //
                            subscriptionPeriodFee.SubscriptionConstantId = subscriptionConstantEntity.Id;
                            subscriptionPeriodFee.Multiplier = multiplier;
                            subscriptionPeriodFee.PeriodFromDate = startDate;
                            subscriptionPeriodFee.PeriodToDate = endDate;

                            subscriptionPeriodFees.Add(subscriptionPeriodFee);



                            var diff = CommonMethods.GetMonthBetweenTwoDate(startDate, endDate) + 1;
                            //
                            for (int i = 0; i < diff; i++)
                            {
                                var subscriptionMonth = new SubscriptionMonth();
                                //
                                subscriptionMonth.Id = Guid.NewGuid();
                                subscriptionMonth.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
                                subscriptionMonth.CreatedDate = DateTime.Now.Date;
                                subscriptionMonth.IsActive = true;

                                var month = startDate.Month + i;
                                subscriptionMonth.SubscriptionPeriodFeeId = subscriptionPeriodFeeId;
                                subscriptionMonth.Month = month;
                                subscriptionMonth.Year = endDate.Year;

                                subscriptionMonths.Add(subscriptionMonth);
                            }


                        }
                        catch (Exception exp1)
                        {

                        }

                    });

                    _db.SubscriptionPeriodFees.AddRange(subscriptionPeriodFees);
                    _db.SubscriptionMonths.AddRange(subscriptionMonths);

                    _db.SaveChanges();

                }

            }
            catch (Exception exp)
            {

            }
        }
    }
}
