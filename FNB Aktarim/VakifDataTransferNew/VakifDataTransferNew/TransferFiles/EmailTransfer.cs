﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VakifDataTransferNew.Common;
using VakifDataTransferNew.Helper;
using VakifDataTransferNew.Model;

namespace VakifDataTransferNew.TransferFiles
{
    public static class EmailTransfer
    {

        public static void AddEmails(string path)
        {
            try
            {

                Log.WriteToFile(@"C:\Log", "EmailUpdate.txt", $"", false);


                var oldEmailList = new List<Email>();
                var personList = new List<Person>();
                var emailType = new List<EmailType>();
                var newDate = DateTime.Now.Date;
                var persCreateDate = new DateTime(2018, 9, 26);
                //
                using (var _db = new FMSEntities())
                {
                    oldEmailList = _db.Emails.ToList();
                    personList = _db.Persons.Where(q => q.CreatedDate >= persCreateDate).ToList();
                    emailType = _db.EmailTypes.ToList();

                    var list = new List<Email>();
                    var lines = File.ReadLines(path).ToList();
                    var count = 0;


                    lines.ForEach(l =>
                    {

                        count++;
                        if (count == 1)
                        {
                            return;
                        }



                        var arrayList = l.Split(';');
                        var registerNo = arrayList[0].ToString();
                        var typeName = arrayList[3].ToString().ToLower().Trim();
                        var type = emailType.FirstOrDefault(q => q.IsActive && q.Name.ToLower().Trim() == typeName);
                        var typeGuid = Guid.Empty;
                        //
                        if (type == null)
                            typeGuid = new Guid("868d92a7-e76a-41b3-818a-d6fda64de9f2");
                        else
                            typeGuid = type.Id;



                        var active = arrayList[1].ToString() != "" ? Convert.ToInt32(arrayList[1].ToString()) : 0;
                        var isActive = active == 1 ? true : false;
                        //
                        var emailName = arrayList[2].ToString();
                        var personEntity = personList.FirstOrDefault(q => q.RegisterNo.Trim() == registerNo.Trim());
                        //
                        if (personEntity == null)
                        {
                            Log.WriteToFile(@"C:\Log", "EmailErro.txt", $"RegisterNo : {registerNo} Bulunamadi - Email Name : {emailName} - Date : {DateTime.Now.Date}", true);
                            return;
                        }

                        try
                        {

                            var email = oldEmailList.FirstOrDefault(q => q.IsActive && q.Name.ToLower().Trim() == emailName.ToLower().Trim());
                            //
                            if (email == null)
                            {
                                email = new Email();
                                //
                                email.Id = Guid.NewGuid();
                                email.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
                                email.CreatedDate = newDate;
                                //
                                email.Name = emailName.ToLower().ConvertTurkishCharToEnglish();
                                email.IsActive = isActive;
                                email.IsDefault = true;
                                email.TypeId = typeGuid;
                                email.PersonId = personEntity.Id;


                                list.Add(email);

                            }
                            else
                            {
                                email.IsActive = isActive;
                                Log.WriteToFile(@"C:\Log", "EmailUpdate.txt", $"RegisterNo : {registerNo} - Email Name : {emailName} - Date : {DateTime.Now.Date}", true);
                                _db.SaveChanges();
                                return;
                            }
 

                        }
                        catch (Exception exp)
                        {

                            throw exp;
                        }


                        

                    });


                    var dtEmail = list.KopyalaToDataTable<Email>();
                    var dict = new Dictionary<string, DataTable>();
                    dict.Add("Emails", dtEmail);
                    BulkProcess.BulkInsert(dict);


                    //_db.Emails.AddRange(list);
                    //_db.SaveChanges();
                }
            }
            catch (Exception exp)
            {

            }
        }

    }
}
