﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VakifDataTransferNew.Common;
using VakifDataTransferNew.Helper;
using VakifDataTransferNew.Model;

namespace VakifDataTransferNew.TransferFiles
{
    public static class MembershipServiceTransfer
    {
        static DateTime newDate = new DateTime(2021, 02, 05);
        public static void AddPhones(string path)
        {
            try
            {
                var personMemberships = new List<vwPersonMembership>();
                var personList = new List<Person>();
                var businessServiceGroups = new List<BusinessServiceGroup>();
                //
                var turkeyGuid = new Guid("eabea8f6-6e57-4427-a62e-6e37a2fa5869");



                var membershipServices = new List<MembershipService>();
                var servicePayments = new List<ServicePayment>();
                var transactions = new List<Transaction>();
                var manualTransactions = new List<ManualTransaction>();
                var accountCards = new List<AccountCard>();
                var accountCardDeposits = new List<AccountCardDeposit>();

                using (var _db = new FMSEntities())
                {
                    personMemberships = _db.vwPersonMemberships.ToList();
                    personMemberships = Set_MembershipNames_Of_TransactionVMs(personMemberships);
                    businessServiceGroups = _db.BusinessServiceGroups.ToList();

                    var getAllAccounts = GetBankAccountsInfo();
                    var getVakifAllAccounts = GetVakifBankAccounts(getAllAccounts);
                    var t = _db.AccountCards.ToList();

                    var lines = File.ReadLines(path).ToList();
                    var count = 0;
                    //
                    lines.ForEach(l =>
                    {
                        count++;
                        if (count == 1)
                        {
                            return;
                        }

                        var arrayList = l.Split(';');
                        //
                        var registerNo = arrayList[0].ToString();
                        var membershipName = arrayList[1].ToString();
                        var groupName = arrayList[2].ToString();
                        var strIsFree = arrayList[3].ToString() != "" ? Convert.ToInt32(arrayList[3].ToString()) : 0;
                        var strAmount = arrayList[4].ToString().Trim();
                        var strApplicationDate = arrayList[5].ToString().Trim();
                        var strActivationDate = arrayList[6].ToString().Trim();
                        var referenceCode = arrayList[7].ToString().Trim();
                        var installmentCount = arrayList[8].ToString().Trim();
                        var strLastPaymentDate = arrayList[9].ToString().Trim();
                        var strIsPaid = arrayList[10].ToString() != "" ? Convert.ToInt32(arrayList[10].ToString()) : 0;
                        var strServiceNo = arrayList[11].ToString().Trim();

                        var isFree = strIsFree == 1 ? true : false;
                        var isPaid = strIsPaid == 1 ? true : false;
                        var amount = strAmount != "" ? Convert.ToDecimal(strAmount) : 0m;

                        var membership = personMemberships.FirstOrDefault(q => q.RegisterNo == registerNo && q.MembershipName == membershipName);
                        if (membership == null)
                        {
                            Log.WriteToFile(@"C:\Log", "MembershipService.txt", $"{count}- RegisterNo: {registerNo} - membership : {membershipName} Bulunamadi", true);
                            return;
                        }


                        var businessServiceGroup = businessServiceGroups.FirstOrDefault(q => q.Name == groupName);
                        if (businessServiceGroup == null)
                        {
                            Log.WriteToFile(@"C:\Log", "MembershipService.txt", $"{count}- RegisterNo: {registerNo} - GroupName : {groupName} Bulunamadi", true);
                            return;
                        }

                        if (strApplicationDate == "")
                        {
                            Log.WriteToFile(@"C:\Log", "MembershipService.txt", $"{count}- RegisterNo: {registerNo} - strApplicationDate : {strApplicationDate} Bulunamadi", true);
                            return;
                        }
                        var applicationDate = Convert.ToDateTime(strApplicationDate);


                        if (strActivationDate == "")
                        {
                            Log.WriteToFile(@"C:\Log", "MembershipService.txt", $"{count}- RegisterNo: {registerNo} - strActivationDate : {strActivationDate} Bulunamadi", true);
                            return;
                        }
                        var activationDate = Convert.ToDateTime(strActivationDate);


                        var membershipService = new MembershipService();
                        CreateMembershipService(ref membershipService, (Guid)membership.Id, businessServiceGroup.Id, applicationDate,
                            activationDate, referenceCode, "Otomatik Kayıt!", amount, amount);

                        membershipServices.Add(membershipService);


                        var servicePayment = new ServicePayment();
                        var transaction = new Transaction();
                        CreateServicePayments(ref servicePayment, membershipService,
                                       amount, activationDate,
                                       getAllAccounts,
                                       getVakifAllAccounts,
                                       ref transaction);

                        servicePayments.Add(servicePayment);
                        transactions.Add(transaction);


                        var accountCard = CreateAccountCard(accountCards, membership.PersonId);
                        accountCards.Add(accountCard);

                        var accountCardDeposite = CreateAccountCardDeposit(amount, accountCard.Id);
                        accountCardDeposits.Add(accountCardDeposite);

                        var manualTransaction = new ManualTransaction();
                        CreateManualTransaction(ref manualTransaction, membershipService, transaction.Id, accountCard.Id, amount);
                        manualTransactions.Add(manualTransaction);
                    });
                }

                var dtMembershipServices = membershipServices.KopyalaToDataTable<MembershipService>();
                var dtservicePayments = servicePayments.KopyalaToDataTable<ServicePayment>();
                var dtTransaction = transactions.KopyalaToDataTable<Transaction>();

                var dtAccountCards = accountCards.KopyalaToDataTable<AccountCard>();
                var dtaAccountCardDeposits = accountCardDeposits.KopyalaToDataTable<AccountCardDeposit>();
                var dtManualTransaction = manualTransactions.KopyalaToDataTable<ManualTransaction>();

                var dict = new Dictionary<string, DataTable>();

                dict.Add("MembershipServices", dtMembershipServices);
                dict.Add("ServicePayments", dtservicePayments);
                dict.Add("Transactions", dtTransaction);

                if(dtAccountCards.Rows.Count > 0)
                    dict.Add("AccountCards", dtAccountCards);

                dict.Add("AccountCardDeposits", dtaAccountCardDeposits);
                dict.Add("ManualTransactions", dtManualTransaction);

                BulkProcess.BulkInsert(dict);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        private static void CreateMembershipService(ref MembershipService membershipService, Guid membershipId, Guid serviceGroupId, DateTime appDate, DateTime actDate, string refCode, string membershipServiceNote, decimal amount, decimal amountNet)
        {

            var msGuid = Guid.NewGuid();
            membershipService.Id = msGuid;
            membershipService.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
            membershipService.CreatedDate = newDate;
            membershipService.IsActive = true;
            membershipService.ApplicationDate = appDate;
            membershipService.ActivationDate = actDate;
            membershipService.ReferenceCode = refCode;
            membershipService.MembershipId = membershipId;
            membershipService.BusinessServiceGroupId = serviceGroupId;
            membershipService.Note = membershipServiceNote;
            membershipService.Amount = amount;
            membershipService.AmountNet = amountNet;
        }

        private static void CreateServicePayments(ref ServicePayment servicePayment, MembershipService membershipService,
                                       decimal amount, DateTime maturityDate, 
                                       List<BankAccountsModel> getAllAccounts,
                                       List<BankAccountsModel> getVakifAllAccounts,
                                       ref Transaction transactions)
        {


            servicePayment.Id = Guid.NewGuid();
            servicePayment.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
            servicePayment.CreatedDate = newDate;
            servicePayment.Amount = amount;
            //servicePayment.MaturityDate = StringToDate_BySeperator(arrayPaymentPlanDate[i], '-');
            servicePayment.MaturityDate = maturityDate;
            servicePayment.MembershipService = membershipService;
            servicePayment.MembershipServiceId = membershipService.Id;
            servicePayment.IsActive = true;
            servicePayment.IsOffTime = false;
            servicePayment.Note = "";

            var transaction = CreateTransaction_ByServicePlan(servicePayment, getAllAccounts, getVakifAllAccounts, "Otomatik Kayıt!");
        }

        private static AccountCard CreateAccountCard(List<AccountCard> accountCards, Guid personId)
        {
            var accountCard = accountCards.FirstOrDefault(q => q.PersonId == personId);
            if (accountCard != null)
                return accountCard;

            accountCard = new AccountCard();

            accountCard.Id = Guid.NewGuid();
            accountCard.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
            accountCard.CreatedDate = newDate;
            accountCard.IsActive = true;
            accountCard.PersonId = personId;

            return accountCard;
        }

        private static AccountCardDeposit CreateAccountCardDeposit(decimal amount, Guid accountCardId)
        {
            var accountCardDeposit = new AccountCardDeposit();
            accountCardDeposit.Id = Guid.NewGuid();
            accountCardDeposit.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
            accountCardDeposit.CreatedDate = newDate;
            accountCardDeposit.IsActive = true;
            accountCardDeposit.Amount = amount;
            accountCardDeposit.AccountCardId = accountCardId;
            accountCardDeposit.TransactionTypeId = new Guid("658eae6b-e7a3-4a5a-bf57-51090dc6f94e");
            accountCardDeposit.IsDeposit = true;
            accountCardDeposit.PaymentDate = DateTime.Now;

            return accountCardDeposit;
        }

        private static void CreateManualTransaction(ref ManualTransaction manualTransaction, MembershipService membershipService, Guid transactionId, Guid accountCardId, decimal amount)
        {
            manualTransaction.Id = Guid.NewGuid();
            manualTransaction.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
            manualTransaction.CreatedDate = DateTime.Now.Date;
            manualTransaction.Amount = amount;
            manualTransaction.TransactionId = transactionId;
            manualTransaction.MembershipService = membershipService;
            manualTransaction.MembershipServiceId = membershipService.Id;
            manualTransaction.AccountCardId = accountCardId;
            manualTransaction.IsActive = true;
            manualTransaction.PaymentDate = DateTime.Now;
            manualTransaction.Note = "";
        }

        public static Transaction CreateTransaction_ByServicePlan(ServicePayment servicePayment, List<BankAccountsModel> getAllAccounts, List<BankAccountsModel> getVakifAllAccounts, string serviceNote)
        {
            var membershipService = servicePayment.MembershipService;
            var businessServiceType = membershipService.BusinessServiceGroup.BusinessServiceType;
            var jobInfo = membershipService.Membership.JobInformation;
            var person = jobInfo.Person;
            var personBankAccount = jobInfo.BankAccounts.FirstOrDefault(q => q.IsDefault);
            //
            var transaction = new Transaction();
            //
            transaction.Id = Guid.NewGuid();
            transaction.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
            transaction.CreatedDate = DateTime.Now.Date;
            transaction.IsActive = true;
            transaction.TypeId = new Guid("48d011a8-2ff5-4d66-ae40-000a89f4ea4b"); // Otomatik Odeme;

            transaction.MaturityDate = servicePayment.MaturityDate;
            transaction.ServicePaymentPlanId = servicePayment.Id;
            transaction.PersonId = person.Id;
            if (personBankAccount != null)
                transaction.FromAccountId = personBankAccount.Id;


            // Find Vakif equivalent
            var vakifAccount = GetVakifBankId(getAllAccounts, getVakifAllAccounts, personBankAccount.Id);
            //
            if (vakifAccount != Guid.Empty)
            {
                transaction.ToAccountId = vakifAccount;
            }


            transaction.Note = $"Otomatik Kayıt!";

            return transaction;
        }

        public static List<BankAccountsModel> GetVakifBankAccounts(List<BankAccountsModel> getAllAccounts)
        {
            try
            {
                var getVakifAllAccounts = getAllAccounts.Where(q => q.OrganisationName != null && q.OrganisationId == new Guid("11FEFB8F-9B6F-4B8A-8D46-9E7702AE790A")).ToList(); // 'TURK HAVAK'
                return getVakifAllAccounts;
            }
            catch (Exception exp)
            {
                throw;
            }
        }

        public static List<BankAccountsModel> GetBankAccountsInfo()
        {
            try
            {
                string sql = $@"Select b.Id BankId, b.Name BankName, br.Id BranchId, br.Name BranchName, ba.Id BankAccountId, ba.AccountNo, ba.IBAN, o.Id OrganisationId, o.Name OrganisationName from BankAccounts ba
                                join (Select * from Branches where IsActive = 1) br on br.Id = ba.BranchId
                                join (Select * from Banks where IsActive = 1) b on b.Id = br.BankId
                                left join (Select * from Organisations where IsActive = 1 ) o on o.Id = ba.OrganisationId
                                where ba.IsActive = 1 ";

                var bankAccountViewModels = new List<BankAccountsModel>();
                using (var connection = new SqlConnection(SqlConnectionBuilder.GetConnectionString("FMSEntities")))
                {
                    bankAccountViewModels = connection.Query<BankAccountsModel>(sql).ToList();
                }

                return bankAccountViewModels;
            }
            catch (Exception exp)
            {

                throw exp;
            }
        }

        public static Guid GetVakifBankId(List<BankAccountsModel> getAllAccounts, List<BankAccountsModel> getVakifAllAccounts, Guid FromAccountId)
        {
            try
            {
                var toAccount = Guid.Empty;
                // Find Vakif equivalent
                var memberBankAccount = getAllAccounts.FirstOrDefault(q => q.BankAccountId == FromAccountId);
                if (memberBankAccount != null)
                {
                    var vakifBankAccount = getVakifAllAccounts.FirstOrDefault(q => q.BankId == memberBankAccount.BankId);
                    if (vakifBankAccount != null)
                        toAccount = vakifBankAccount.BankAccountId;
                }

                return toAccount;
            }
            catch (Exception exp)
            {
                throw;
            }
        }

        private static DateTime StringToDate_BySeperator(string strDate, char seperator)
        {

            var format = strDate.Split(seperator);
            //
            var year = Convert.ToInt32(seperator == '-' ? format[0] : format[2]);
            var month = Convert.ToInt32(format[1]);
            var day = Convert.ToInt32(seperator == '-' ? format[02] : format[0]);

            var date = new DateTime(year, month, day);

            return date;

        }



        public static List<vwPersonMembership> Set_MembershipNames_Of_TransactionVMs(List<vwPersonMembership> vm)
        {

            var regNo = "";
            var count = 0;

            vm = vm.OrderBy(q => q.RegisterNo)
                   //.ThenBy(q => q.MaturityDate)
                   .ThenBy(q => q.MembershipStartDate)
                   .ToList();


            var maturityDate = DateTime.MinValue;
            for (int i = 0; i < vm.Count; i++)
            {

                if (vm[i].RegisterNo == null)
                    continue;

                if (vm[i].RegisterNo == "13892")
                {

                }

                if (regNo != "" && regNo == vm[i].RegisterNo)
                {
                    //if (vm[i].MaturityDate.Date == maturityDate)
                    //    count++;
                    //else
                    //    count = 0;
                }
                else
                    count = 0;

                //maturityDate = vm[i].MaturityDate.Date;

                if (vm[i].Ratio < 100)
                {
                    vm[i].MembershipName = $"%{vm[i].Ratio} Uyelik";
                    regNo = vm[i].RegisterNo;


                    if (count > 0)
                    {
                        // eger ikinci yada ucuncu %50 gelirse dogru indexi bulmak icin count-- yapiyoruz
                        count--;
                    }
                    else
                    {
                        // eger ilk %50 gelirse asil uyelik ve artilari bulmak icin regNo = "" yapiyoruz
                        regNo = "";
                    }

                    continue;
                }

                if (count == 0)
                {
                    vm[i].MembershipName = "Asil Uyelik";
                    regNo = vm[i].RegisterNo;

                    continue;
                }

                vm[i].MembershipName = $"+{count} Uyelik";
                regNo = vm[i].RegisterNo;
            }


            return vm;

        }
    }

    public class BankAccountsModel
    {
        public Guid BankId { get; set; }
        public string BankName { get; set; }
        public Guid BranchId { get; set; }
        public string BranchName { get; set; }
        public Guid BankAccountId { get; set; }
        public string AccountNo { get; set; }
        public string IBAN { get; set; }
        public Guid OrganisationId { get; set; }
        public string OrganisationName { get; set; }
    }
}
