﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VakifDataTransferNew.Common;
using VakifDataTransferNew.Helper;
using VakifDataTransferNew.Model;

namespace VakifDataTransferNew.TransferFiles
{
    public static class PersonTransfer
    {

        public static void TransferInfoToPersone(string path)
        {
            try
            {
                var list = new List<Person>();
                var lines = File.ReadLines(path).ToList();
                var count = 0;

                
                using (var _db = new FMSEntities())
                {

                    var genders = _db.Genders.Where(q => q.IsActive).ToList();
                    var bloodGroups = _db.BloodGroups.Where(q => q.IsActive).ToList();
                    //
                    lines.ForEach(l =>
                    {

                        count++;
                        if (count == 1)
                        {
                            return;
                        }


                        var arrayList = l.Split(';');
                        var active = arrayList[1].ToString() != "" ? Convert.ToInt32(arrayList[1].ToString()) : 0;
                        var isActive = active == 1 ? true : false;
                        var genderName = arrayList[6].ToString().Trim() != "" ? arrayList[6].ToString().Trim() : "Belirtilmemiş";
                        var bloodType = arrayList[7].ToString().Trim() != "" ? arrayList[7].ToString().Trim() : "Belirtilmemiş";
                        

                        var gender = genders.FirstOrDefault(q => q.IsActive && q.Name.ToLower().Trim() == genderName.ToLower());
                        var genderId = gender != null ? gender.Id : new Guid("a948b784-e35b-401b-96d8-a61df45f9231");
                        //
                        var bloodGroup = bloodGroups.FirstOrDefault(q => q.IsActive && q.Name.ToLower().Trim() == bloodType.ToLower());
                        var bloodGroupId = bloodGroup != null ? bloodGroup.Id : new Guid("cd982a6e-586f-4a85-bc15-31ebf82be338");

                        try
                        {
                            var person = new Person();
                            //
                            person.Id = Guid.NewGuid();
                            person.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
                            person.CreatedDate = DateTime.Now.Date;
                            person.IsActive = isActive;
                            //
                            person.RegisterNo = arrayList[0].ToString().Trim();
                            person.NationalId = arrayList[2].ToString().Trim();
                            person.FirstName = arrayList[3].ToString().Trim();
                            person.LastName = arrayList[4].ToString().Trim();
                            person.BirthDate = arrayList[5] != null && 
                                               arrayList[5].ToString().Trim() != "" &&
                                               arrayList[5].ToString().Trim() != "NULL" ? Convert.ToDateTime(arrayList[5]) : new DateTime(1753, 1, 1);
                            person.GenderId = genderId;
                            person.BloodId = bloodGroupId;

                            list.Add(person);
                        }
                        catch (Exception exp)
                        {

                            throw exp;
                        }

                    });



                    var dtPerson = list.KopyalaToDataTable<Person>();

                    var dict = new Dictionary<string, DataTable>();
                    dict.Add("Persons", dtPerson);
                    BulkProcess.BulkInsert(dict);

                    // BulkProcess.BulkInsert(dtPerson, "Persons");
                }
                
            }
            catch (Exception exp)
            {

                throw exp;
            }
        }
    }
}
