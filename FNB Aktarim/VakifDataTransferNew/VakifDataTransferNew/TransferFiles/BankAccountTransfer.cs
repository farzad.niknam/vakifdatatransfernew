﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VakifDataTransferNew.Common;
using VakifDataTransferNew.Helper;
using VakifDataTransferNew.Model;

namespace VakifDataTransferNew.TransferFiles
{
    public static class BankAccountTransfer
    {

        public static void TransferInfoToBankAccount(string path)
        {
            try
            {

                Log.WriteToFile(@"C:\Log", "BankAccount.txt", $"", false);


                using (var _db = new FMSEntities())
                {

                    var lines = File.ReadLines(path).ToList();
                    var count = 0;

                    
                    var jobInformations = _db.JobInformations.AsNoTracking().ToList();
                    var branches = _db.Branches.AsNoTracking().ToList();
                    var persons = _db.Persons.AsNoTracking().ToList();

                    var bankAccounts = new List<BankAccount>();
                    //
                    lines.ForEach(l =>
                    {
                        try
                        {

                            count++;
                            if (count == 1)
                            {
                                return;
                            }



                            
                            var arrayList = l.Split(';');
                            var registerNo = arrayList[0].ToString().Trim();
                            //
                            var jobInformationEntity = jobInformations.FirstOrDefault(x => x.RegisterNo.ToLower().Trim() == registerNo.ToLower().Trim());
                            //
                            if (jobInformationEntity == null)
                            {
                                Log.WriteToFile(@"C:\Log", "BankAccount.txt", $"JobInfo RegisterNo : {registerNo} Bulunamadi", true);
                                // return;
                            }


                            var personEntity = persons.FirstOrDefault(x => x.RegisterNo.ToLower().Trim() == registerNo.ToLower().Trim());
                            //
                            if (personEntity == null)
                            {
                                Log.WriteToFile(@"C:\Log", "BankAccount.txt", $"Person RegisterNo : {registerNo} Bulunamadi", true);
                                return;
                            }



                            var subeAdi = arrayList[1].ToString().ToLower().Trim();
                            //
                            var branchEntity = branches.FirstOrDefault(x => x.Name.ToLower().Trim() == subeAdi);
                            //
                            if (branchEntity == null)
                            {
                                Log.WriteToFile(@"C:\Log", "BankAccount.txt", $"Sube Adi : {subeAdi} Bulunamadi", true);
                                return;
                            }


                            var active = arrayList[2].ToString() != "" ? Convert.ToInt32(arrayList[2].ToString()) : 0;
                            var isActive = active == 1 ? true : false;
                            //
                            var accountNumber = arrayList[3].ToLower().ToString().Trim();


                            var iban = arrayList[4].ToString().Trim();


                            var bankAccount = new BankAccount();
                            //
                            bankAccount.Id = Guid.NewGuid();
                            bankAccount.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
                            bankAccount.CreatedDate = DateTime.Now.Date;
                            bankAccount.IsActive = isActive;
                            //
                            bankAccount.AccountNo = accountNumber;
                            bankAccount.IsDefault = true;
                            bankAccount.BranchId = branchEntity.Id;
                            bankAccount.IBAN = iban;
                            bankAccount.OriginId = Guid.Empty;

                            // Hangisini doldurmamiz lazim : personId or jobInformationId ????????????????????????
                            //
                            bankAccount.JobInformationId = jobInformationEntity.Id;
                            // bankAccount.OrganisationId = Guid.NewGuid();
                            bankAccount.PersonId = personEntity.Id;

                            bankAccounts.Add(bankAccount);

                        }
                        catch (Exception exp1)
                        {

                        }

                    });

                    try
                    {

                        var dtBankAccounts = bankAccounts.KopyalaToDataTable<BankAccount>();
                        var dict = new Dictionary<string, DataTable>();
                        dict.Add("BankAccounts", dtBankAccounts);
                        BulkProcess.BulkInsert(dict);

                        //_db.BankAccounts.AddRange(bankAccounts);
                        //_db.SaveChanges();
                    }
                    catch (Exception exp)
                    {

                        throw exp;
                    }
                }

            }
            catch (Exception exp)
            {

            }
        }

    }
}
