﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VakifDataTransferNew.Common;
using VakifDataTransferNew.Model;

namespace VakifDataTransferNew.TransferFiles
{
    public class SubscriptionConstantTransfer
    {

        public static void TransferInfoToSubscriptionConstant(string path)
        {
            try
            {

                Log.WriteToFile(@"C:\Log", "SubscriptionConstant.txt", $"", false);


                using (var _db = new FMSEntities())
                {

                    var lines = File.ReadLines(path).ToList();
                    var count = 0;

                    var subscriptionConstants = new List<SubscriptionConstant>();
                    //
                    lines.ForEach(l =>
                    {
                        try
                        {

                            count++;
                            if (count == 1)
                            {
                                return;
                            }

                            var arrayList = l.Split(';');
                            var effectiveDateStr = arrayList[0].ToString().Trim();
                            //
                            if(effectiveDateStr == "")
                            {
                                Log.WriteToFile(@"C:\Log", "SubscriptionConstant.txt", $"EffectiveDateStr : {effectiveDateStr} Bulunamadi", true);
                                return;
                            }


                            var effectiveDate = Convert.ToDateTime(effectiveDateStr);
                            //
                            var indicatorStr = arrayList[1].ToString().Trim();
                            var multiplierStr = arrayList[2].ToString().Trim();
                            var divisorStr = arrayList[3].ToString().Trim();
                            var kaysayiStr = arrayList[4].ToString().Trim();
                            //
                            var indicator = indicatorStr != "" ? Convert.ToDouble(indicatorStr) : 0d;
                            var multiplier = multiplierStr != "" ? Convert.ToDouble(multiplierStr) : 0d;
                            var divisor = divisorStr != "" ? Convert.ToDouble(divisorStr) : 0d;
                            var kaysayi = kaysayiStr != "" ? Convert.ToDouble(kaysayiStr) : 0d;


                            var subscriptionConstant = new SubscriptionConstant();
                            //
                            subscriptionConstant.Id = Guid.NewGuid();
                            subscriptionConstant.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
                            subscriptionConstant.CreatedDate = DateTime.Now.Date;
                            subscriptionConstant.IsActive = true;
                            //
                            subscriptionConstant.Divisor = divisor;
                            subscriptionConstant.EffectiveDate = effectiveDate;
                            subscriptionConstant.Indicator = indicator;
                            subscriptionConstant.Katsayi = kaysayi;
                            subscriptionConstant.Multiplier = multiplier;

                            subscriptionConstants.Add(subscriptionConstant);

                        }
                        catch (Exception exp1)
                        {

                        }

                    });

                    _db.SubscriptionConstants.AddRange(subscriptionConstants);
                    _db.SaveChanges();
                }

            }
            catch (Exception exp)
            {

            }
        }

    }
}
