﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VakifDataTransferNew.Common;
using VakifDataTransferNew.Helper;
using VakifDataTransferNew.Model;

namespace VakifDataTransferNew.TransferFiles
{
    public static class PhoneTransfer
    {

        public static void AddPhones(string path)
        {
            try
            {

                Log.WriteToFile(@"C:\Log", "Address.txt", $"", false);

                var phoneList = new List<PhoneNumber>();
                var personList = new List<Person>();
                //
                var turkeyGuid = new Guid("eabea8f6-6e57-4427-a62e-6e37a2fa5869");
                var newDate = DateTime.Now.Date;
                var persCreateDate = new DateTime(2018, 9, 26);
                //
                using (var _db = new FMSEntities())
                {

                    personList = _db.Persons.Where(q => q.CreatedDate >= persCreateDate).ToList();
                    //
                    var lines = File.ReadLines(path).ToList();
                    var count = 0;
                    //
                    lines.ForEach(l =>
                    {
                        try
                        {

                            count++;
                            if (count == 1)
                            {
                                return;
                            }

                            var arrayList = l.Split(';');
                            //
                            var registerNo = arrayList[0].ToString();
                            var active = arrayList[1].ToString() != "" ? Convert.ToInt32(arrayList[1].ToString()) : 0;
                            var isActive = active == 1 ? true : false;
                            //
                            var number = arrayList[2].ToString();
                            var d = arrayList[3].ToString() != "" ? Convert.ToInt32(arrayList[3].ToString()) : 0;
                            var isDefault = d == 1 ? true : false;


                            var personEntity = personList.FirstOrDefault(q => q.RegisterNo.Trim() == registerNo.Trim());
                            //
                            if (personEntity == null)
                            {
                                Log.WriteToFile(@"C:\Log", "Address.txt", $"RegisterNo : {registerNo} Bulunamadi - Date : {DateTime.Now.Date}", true);
                                return;
                            }


                            var phone = new PhoneNumber();
                            //
                            phone.Id = Guid.NewGuid();
                            phone.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
                            phone.CreatedDate = newDate;
                            //
                            phone.IsActive = isActive;
                            phone.Number = number;
                            phone.CountryId = turkeyGuid;
                            phone.PersonId = personEntity.Id;
                            phone.IsDefault = isDefault;
                            phone.TypeId = new Guid("cec07f6e-ecea-45a2-9818-5e7abd94420a");


                            phoneList.Add(phone);

                        }
                        catch (Exception exp)
                        {

                            throw exp;
                        }


                    });

                    var dtPhoneNumber = phoneList.KopyalaToDataTable<PhoneNumber>();
                    var dict = new Dictionary<string, DataTable>();
                    dict.Add("PhoneNumbers", dtPhoneNumber);
                    BulkProcess.BulkInsert(dict);


                    //_db.PhoneNumbers.AddRange(phoneList);
                    //_db.SaveChanges();
                }

            }
            catch (Exception exp)
            {

                throw exp;
            }
        }

    }
}
