﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VakifDataTransferNew.Common;
using VakifDataTransferNew.Helper;
using VakifDataTransferNew.Model;

namespace VakifDataTransferNew.TransferFiles
{
    public static class JobInformationTransfer
    {
        public static void TransferInfoToJobInfo(string path)
        {
            try
            {

                Log.WriteToFile(@"C:\Log", "JobInformation.txt", $"", false);


                using (var _db = new FMSEntities())
                {

                    var lines = File.ReadLines(path).ToList();
                    var count = 0;

                    var jobStatuses = _db.JobStatuses.AsNoTracking().ToList();
                    var organisations = _db.Organisations.AsNoTracking().ToList();
                    var persons = _db.Persons.AsNoTracking().ToList();

                    var jobInformations = new List<JobInformation>();
                    //
                    lines.ForEach(l =>
                    {
                        try
                        {

                            count++;
                            if (count == 1)
                            {
                                return;
                            }

                            var arrayList = l.Split(';');
                            var organisationNames = arrayList[0].ToString().Split('/');
                            //
                            if (organisationNames.Count() != 4)
                            {
                                Log.WriteToFile(@"C:\Log", "JobInformation.txt", $"{arrayList[1].ToString()};{count}", true);
                                return;
                            }



                            var active = arrayList[1].ToString() != "" ? Convert.ToInt32(arrayList[1].ToString()) : 0;
                            var isActive = active == 1 ? true : false;
                            //
                            var jobStatusName = arrayList[2].ToLower().ToString().Trim();
                            var jobStatusEntity = jobStatuses.FirstOrDefault(x => x.Name.ToLower().Trim() == jobStatusName);
                            var jobStatusId = Guid.Empty;
                            //
                            if (jobStatusEntity == null)
                                jobStatusId = new Guid("d88d93e1-7e54-42bf-8852-47a0a81c7f6e");
                            else
                                jobStatusId = jobStatusEntity.Id;


                            var registerNo = arrayList[3].ToString().Trim();
                            var personeEntity = persons.FirstOrDefault(x => x.RegisterNo == registerNo);
                            //
                            if(personeEntity == null)
                            {
                                Log.WriteToFile(@"C:\Log", "JobInformation.txt", $"RegisterNo : {registerNo} Bulunamadi - Date : {DateTime.Now.Date}", true);
                                return;
                            }


                            //
                            var startDate =  arrayList[4].ToString().Trim();
                            var endDate = arrayList[5].ToString().Trim();
                            var paymentDay = arrayList[6].ToString().Trim();
                            //
                            var organisationName = "";
                            //
                            for (int i = 4; i > 0; --i)
                            {

                                if (organisationNames[i-1].ToString().Trim() == "-")
                                    continue;

                                organisationName = organisationNames[i-1].ToString().Trim();
                                break;
                            }

                            if(organisationName == "")
                                organisationName = "Belirtilmemiş";


                            var organisationEntity = organisations.FirstOrDefault(x => x.Name.ToLower().Trim() == organisationName.ToLower().Trim());
                            //
                            if(organisationEntity == null)
                            {
                                Log.WriteToFile(@"C:\Log", "JobInformation.txt", $"RegisterNo : {registerNo} Bulunamadi - Organisation : {organisationNames}", true);
                                return;
                            }


                            //
                            if (organisationNames.Count() != 4)
                            {
                                Log.WriteToFile(@"C:\Log", "JobInformation.txt", $"{arrayList[1].ToString()};{count}", true);
                                return;
                            }



                            var jobInfo = new JobInformation();
                            //
                            jobInfo.Id = Guid.NewGuid();
                            jobInfo.CreatedBy = "a5242f09-99f6-4c4d-9bfb-05cfa5e3f571";
                            jobInfo.CreatedDate = DateTime.Now.Date;
                            jobInfo.IsActive = isActive;
                            //
                            if (endDate != "")
                                jobInfo.EndDate = Convert.ToDateTime(endDate);


                            jobInfo.StartDate = startDate != "" ? Convert.ToDateTime(startDate) : new DateTime(1753, 1, 1);
                            jobInfo.JobStatusId = jobStatusId;
                            jobInfo.OrganisationId = organisationEntity.Id;
                            jobInfo.PaymentDay = paymentDay != "" ? Convert.ToInt32(paymentDay) : 0;
                            jobInfo.RegisterNo = registerNo;
                            jobInfo.PersonId = personeEntity.Id;

                            jobInformations.Add(jobInfo);

                        }
                        catch (Exception exp1)
                        {
                            Log.WriteToFile(@"C:\Log", "JobInformation.txt", $"{l}", true);
                        }

                    });

                    var dtJobInformations = jobInformations.KopyalaToDataTable<JobInformation>();
                    var dict = new Dictionary<string, DataTable>();
                    dict.Add("JobInformations", dtJobInformations);
                    BulkProcess.BulkInsert(dict);

                    //_db.JobInformations.AddRange(jobInformations);
                    //_db.SaveChanges();
                }

            }
            catch (Exception exp)
            {

            }
        }

    }
}
